#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//File Handler
#include "FileHandler.h"

//UART defines
#define PORT0 "/dev/ttyUSB0"
#define PORT1 "/dev/ttyUSB1"
#define PORT2 "/dev/ttyUSB2"
#define BAUD_RATE 57600
//SOCKET defines
#define PORT 4000
#define SA struct sockaddr

#define NUMBER_OF_NODES 25
char node_maps[NUMBER_OF_NODES + 1];
struct data_packet
{
    char cmd[7];
    char node_id[4];
    char seqno[6];
} __attribute__((__packed__));

int fd_port = 0;
int fd_port1 = 0;
int fd_port2 = 0;

int main()
{
	
	unsigned char uart_buffer[128], uart1_buffer[128], uart2_buffer[128];
	char input;
	static uint16_t index = 0;
	static uint16_t index1 = 0;
	static uint16_t index2 = 0;
	uint8_t start_protocol =0;
	uint8_t number_of_node = 0;
	uint8_t is_sended = 0;
	FILE *logAP, *logAP1, *logAP2;
	FILE *graph_info, *data_info;
	remove("./graph_info.txt");
	remove("./data_info.txt");
	remove("./logAP.txt");
	remove("./logAP1.txt");
	remove("./logAP2.txt");
	memset(&node_maps, 0, sizeof(node_maps));
	node_maps[NUMBER_OF_NODES] = '\0';
	fd_port = cnlab_open_serial_line(PORT0, BAUD_RATE);
	fd_port1 = cnlab_open_serial_line(PORT1, BAUD_RATE);
	fd_port2 = cnlab_open_serial_line(PORT2, BAUD_RATE);
	if(fd_port == -1 || fd_port1 == -1 || fd_port2 == -1){
		printf("SERIAL COMMUNICATION HAS PROBLEM! PLEASE TRY TO CONNECT AGAIN!\r\n");
		return 0;
	}
	printf("PORT0 = %d, PORT1 = %d, PORT2 = %d\r\n", fd_port, fd_port1, fd_port2);
	int sockfd, connfd;
	struct sockaddr_in servaddr, cli;

	while(1){
		int size1 = serialDataAvail(fd_port);
		int size2 = serialDataAvail(fd_port1);
		int size3 = serialDataAvail(fd_port2);

        if(size1 != 0)
        {
            for(int i = 0; i < size1; i++)
            {
                input = serialGetchar(fd_port);
                uart_buffer[index++] = input;
                if(input == '\n')
                {
					uart_buffer[index] = '\0';
					logAP = fopen("./logAP.txt", "a");
					printf("PORT0: %s\r\n", uart_buffer);
					fwrite(uart_buffer, strlen(uart_buffer), 1, logAP);
					fclose(logAP);
					if(strstr(uart_buffer, "WAITING \'START\' CMD FROM SERVER") != NULL) {
						start_protocol = 1;
					}
					if(strstr(uart_buffer, "?NI") != NULL) {
						char *token = strtok(uart_buffer, "/");
						token = strtok(NULL, "");
						char node_value = token[0];
						if(strchr(node_maps, node_value) == NULL){
							node_maps[number_of_node++] = node_value;
							graph_info = fopen("./graph_info.txt", "a");
							fwrite(token, strlen(token), 1, graph_info);
							fclose(graph_info);
						}
					}
					index = 0;
				}
			}
		}else if(size2 != 0)
		{
            for(int i = 0; i < size2; i++)
            {
                input = serialGetchar(fd_port1);
                uart1_buffer[index1++] = input;
                if(input == '\n')
                {
					uart1_buffer[index1] = '\0';
					logAP1 = fopen("./logAP1.txt", "a");
					printf("PORT1: %s\r\n", uart1_buffer);
					fwrite(uart1_buffer, strlen(uart1_buffer), 1, logAP1);
					fclose(logAP1);
					if(strstr(uart1_buffer, "?NI") != NULL) {
						char *token = strtok(uart1_buffer, "/");
						token = strtok(NULL, "");
						char node_value = token[0];
						if(strchr(node_maps, node_value) == NULL){
							node_maps[number_of_node++] = node_value;
							graph_info = fopen("./graph_info.txt", "a");
							fwrite(token, strlen(token), 1, graph_info);
							fclose(graph_info);
						}
					}
                    index1 = 0;
				}
			}
		}else if(size3 != 0)
		{
			for(int i = 0; i < size3; i++)
			{
				input = serialGetchar(fd_port2);
                uart2_buffer[index2++] = input;
                if(input == '\n')
                {
					uart2_buffer[index2] = '\0';
					logAP2 = fopen("./logAP2.txt", "a");
					printf("PORT2: %s\r\n", uart2_buffer);
					fwrite(uart2_buffer, strlen(uart2_buffer), 1, logAP2);
					fclose(logAP2);
					if(strstr(uart2_buffer, "?NI") != NULL) {
						char *token = strtok(uart2_buffer, "/");
						token = strtok(NULL, "");
						char node_value = token[0];
						if(strchr(node_maps, node_value) == NULL){
							node_maps[number_of_node++] = node_value;
							graph_info = fopen("./graph_info.txt", "a");
							fwrite(token, strlen(token), 1, graph_info);
							fclose(graph_info);
						}
					}
                    index2 = 0;
				}
			}
		}else{
			if(start_protocol){
				sockfd = socket(AF_INET, SOCK_STREAM, 0);
				if(sockfd == -1){
					printf("socket creation failed\r\n");
				}else{
					printf("Socket successfully created\r\n");
				}
				bzero(&servaddr, sizeof(servaddr));
				servaddr.sin_family = AF_INET;
				servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
				servaddr.sin_port = htons(PORT);
				if(connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0){
					printf("connection failed\r\n");
					return 0;
				}else{
					printf("connected to server\r\n");
				}
				char buffer[5];
				while(1){
					read(sockfd, buffer, sizeof(buffer));
					printf("Received from Server %s\n", buffer);
					if(strncmp(buffer, "START", 5) == 0){
						break;
					}
				}
				uint16_t size = cnlab_uart_send_bytes(fd_port, buffer, sizeof(buffer));
				printf("UART sent %d bytes\r\n", size);
				start_protocol = 0;
			}
		}

		if(number_of_node == NUMBER_OF_NODES){
			if(!is_sended){
				send(sockfd, "NETWORK", sizeof("NETWORK"),0);
				printf("Send NETWORK to Server\n");
				is_sended = 1;
			}
			char buffer[10];
			read(sockfd, buffer, sizeof(buffer));
			printf("Received from Server %s\n", buffer);
			if(strncmp(buffer, "SCHEDULING", 10) == 0){
				processFiles();
				break;
			}
		}
	}
	struct data_packet data_pac;
	memset(&data_pac, 0, sizeof(data_pac));
	while(1){
		int size1 = serialDataAvail(fd_port);
		int size2 = serialDataAvail(fd_port1);
		int size3 = serialDataAvail(fd_port2);

        if(size1 != 0)
        {
            for(int i = 0; i < size1; i++)
            {
                input = serialGetchar(fd_port);
                uart_buffer[index++] = input;
                if(input == '\n')
                {
					uart_buffer[--index] = '\0';
					memcpy(&data_pac, uart_buffer, sizeof(data_pac));
					if (strncmp(data_pac.cmd, "?DATA",5)==0){
						snprintf(uart_buffer, sizeof(uart_buffer),"%s, %s, %s\n", data_pac.cmd, data_pac.node_id, data_pac.seqno);
						data_info = fopen("./data_info.txt", "a");
						fwrite(uart_buffer, strlen(uart_buffer), 1, data_info);
						fclose(data_info);
					}else{
						logAP = fopen("./logAP.txt", "a");
						fwrite(uart_buffer, strlen(uart_buffer), 1, logAP);
						fclose(logAP);
					}
					printf("PORT0: %s\r\n", uart_buffer);
					index = 0;
				}
			}
		}else if(size2 != 0)
		{
            for(int i = 0; i < size2; i++)
            {
                input = serialGetchar(fd_port1);
                uart1_buffer[index1++] = input;
                if(input == '\n')
                {
					uart1_buffer[--index1] = '\0';
					memcpy(&data_pac, uart1_buffer, sizeof(data_pac));
					if (strncmp(data_pac.cmd, "?DATA",5)==0){
						snprintf(uart1_buffer, sizeof(uart1_buffer),"%s, %s, %s\n", data_pac.cmd, data_pac.node_id, data_pac.seqno);
						data_info = fopen("./data_info.txt", "a");
						fwrite(uart1_buffer, strlen(uart1_buffer), 1, data_info);
						fclose(data_info);
					}else{
						logAP1 = fopen("./logAP1.txt", "a");
						fwrite(uart1_buffer, strlen(uart1_buffer), 1, logAP1);
						fclose(logAP1);
					}
					printf("PORT1: %s\r\n", uart1_buffer);
                    index1 = 0;
				}
			}
		}else if(size3 != 0)
		{
			for(int i = 0; i < size3; i++)
			{
				input = serialGetchar(fd_port2);
                uart2_buffer[index2++] = input;
                if(input == '\n')
                {
					uart2_buffer[--index2] = '\0';
					memcpy(&data_pac, uart2_buffer, sizeof(data_pac));
					if (strncmp(data_pac.cmd, "?DATA",5)==0){
						snprintf(uart2_buffer, sizeof(uart2_buffer),"%s, %s, %s\n", data_pac.cmd, data_pac.node_id, data_pac.seqno);
						data_info = fopen("./data_info.txt", "a");
						fwrite(uart2_buffer, strlen(uart2_buffer), 1, data_info);
						fclose(data_info);
					}else{
						logAP2 = fopen("./logAP2.txt", "a");
						fwrite(uart2_buffer, strlen(uart2_buffer), 1, logAP2);
						fclose(logAP2);
					}
					printf("PORT2: %s\r\n", uart2_buffer);
                    index2 = 0;
				}
			}
		}
	}
	return 0;
}
