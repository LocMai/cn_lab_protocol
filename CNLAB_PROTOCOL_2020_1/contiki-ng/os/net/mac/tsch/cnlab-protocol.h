/* CNLAB PROTOCOL implementation header.
 * 
 * Author: Huy
 * Date: 2019/09/20
 */
#ifndef __CNLAB_PROTOCOL__
#define __CNLAB_PROTOCOL__

/********** Includes **********/
#include "contiki.h"
#include "sys/node-id.h"
#include "net/nbr-table.h"
#include "net/linkaddr.h"

#define NUMBER_OF_PROBE 250
#define MAX_DEPTH 12
#define MIN_PRR_LINK 0.8
#define RSSI_THRESHOLD -80

enum CNLAB_PROTOCOL_STATES {
  CNLAB_PROTOCOL_DEFAULT,
  CNLAB_PROTOCOL_INITIALIZE,
  CNLAB_PROTOCOL_MEASURE_LINK_QUALITY,
  CNLAB_PROTOCOL_MAKE_SCHEDULE,
  CNLAB_PROTOCOL_SEND_DATA,
  CNLAB_PROTOCOL_RESET
};

enum CNLAB_NODE_TYPES {
  AP = 1,
  NODE = 2
};

enum CNLAB_PACKET_TYPES {
  ROUTING_PACKET,
  SCHEDULING_PACKET
};

typedef uint32_t fixed_point_t;

struct node_info {
  uint16_t id;
  uint8_t type;
  uint8_t level;
  fixed_point_t rank;
  uint8_t degree;
  uint16_t primary_parent;
  uint16_t reserve_parent;
}__attribute__((__packed__));
typedef struct node_info node_info_t;

struct nbr_info {
  uint16_t id;
  uint8_t level;
  uint16_t rx_probe_count;
  int16_t avg_rssi;
  fixed_point_t prr_bwd; //Neighbor->Node
  fixed_point_t prr_fwd; //Node->Neighbor
}__attribute__((__packed__));
typedef struct nbr_info nbr_info_t;

struct uptree_info {
  uint8_t node;
  fixed_point_t rank;
  uint8_t primary_parent;
  uint8_t reserve_parent;
}__attribute__((__packed__));
typedef struct uptree_info uptree_info_t;

struct cell {
  uint16_t timeslot;
  uint8_t channel;
  linkaddr_t lladdr;
}__attribute__((__packed__));
typedef struct cell cell_t;

struct pcllf_schedule {
  uint16_t sf_size;
  uint16_t num_transmit_cells;
  uint16_t num_receive_cells;
  cell_t *transmit_cells;
  cell_t *receive_cells;
}__attribute__((__packed__));
typedef struct pcllf_schedule pcllf_schedule_t;

struct data {
  char cmd[7];
  char node_id[4];
  char seq_no[6];
}__attribute__((__packed__));
typedef struct data data_t;
/***** Variables *****/
//process_event_t send_data_event;
/***** External Variables *****/

/***** CNLAB TSCH Processes *****/
PROCESS_NAME(cnlab_measure_link_process);
PROCESS_NAME(cnlab_receive_network_info);
PROCESS_NAME(cnlab_send_data_process);

/********** Functions *********/
void cnlab_write_log_to_file(const unsigned char *data, unsigned int len);
void cnlab_protocol_init();
void cnlab_process_start();
void cnlab_probe_message_input();
void cnlab_prr_message_input();
void cnlab_node_information_input();
void cnlab_routing_data_input();
void cnlab_pcllf_data_input();
void cnlab_data_input();
void cnlab_protocol_reset();

#endif /* __CNLAB_PROTOCOL__ */