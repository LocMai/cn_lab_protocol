/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A very simple Contiki application showing how Contiki programs look
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "dev/i2c.h"
#include <stdio.h> /* For printf() */
/*---------------------------------------------------------------------------*/
PROCESS(hello_world_process, "Hello world process");
AUTOSTART_PROCESSES(&hello_world_process);
/*---------------------------------------------------------------------------*/
static void
putFloat(uint16_t value)
{
  uint16_t head = value/100;
  uint16_t tail = value - head*100;
  printf("%d.%02d\r\n",head,tail);
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(hello_world_process, ev, data)
{
  float SensorTemp;				// Sensor Temperature
	float ObjTemp;					// Object Temperature
  uint8_t buf[6];
  uint16_t raw_temp[2];

  PROCESS_BEGIN();
  clock_delay(1000000);
  i2c_init(I2C_SDA_PORT, I2C_SDA_PIN, I2C_SCL_PORT, I2C_SCL_PIN, I2C_SCL_NORMAL_BUS_SPEED);
  while (1)
  {
    if(i2c_single_send(0x10, 0x80) == I2C_MASTER_ERR_NONE) {
        if(i2c_burst_receive(0x10, buf, 6) == I2C_MASTER_ERR_NONE) {
          raw_temp[0] = buf[0] + buf[1]*256 + buf[2]*65536;
          raw_temp[1] = buf[3] + buf[4]*256 + buf[5]*65536;
          if(buf[2] < 0x80){
            SensorTemp = (float)raw_temp[0] / 200.0;
          }else{
            SensorTemp = ( (float)(raw_temp[0]-16777216) ) / 200.0;
          }
          if(buf[5] < 0x80){
            ObjTemp = (float)raw_temp[1] / 200.0;
          }else{
            ObjTemp = ( (float)(raw_temp[1]-16777216) ) / 200.0;
          }
          printf("Raw Byte : 0x%X,",buf[0]);
          printf("0x%X,",buf[1]);
          printf("0x%X,",buf[2]);
          printf("0x%X,",buf[3]);
          printf("0x%X,",buf[4]);
          printf("0x%X\r\n",buf[5]);

          printf("Sensor Temp: "); 
          putFloat(SensorTemp*100);
          printf("Object Temp: ");
          putFloat(ObjTemp*100);
          printf("\r\n");
        }
    }
    clock_delay(500000);
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
