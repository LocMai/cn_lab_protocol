/* CNLAB PROTOCOL implementation source.
 * 
 * Author: Huy
 * Date: 2019/09/20
 */
#include "cnlab-protocol.h"
#include "net/mac/tsch/tsch.h"
#include "net/packetbuf.h"
#include "lib/heapmem.h"
#include "lib/random.h"
#include "dev/uart.h"
#include "dev/leds.h"
#include "dev/serial-line.h"
#include "lib/fs/fat/ff.h"
#include <stdlib.h>
#include <math.h>
/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "CNLAB"
#define LOG_LEVEL LOG_LEVEL_DBG
#define LOG_FILENAME   "zoul_log.txt"

#define CLOCK_MILISEC CLOCK_SECOND/1000
/* Fixed Point Handler */
#define ACCURACY 2
#define SHIFT_AMOUNT 16
#define SHIFT_MASK ((1 << SHIFT_AMOUNT) - 1)

/* UART Command Define */
#define NODE_INFO "?NI"
#define END "?END"
#define RESPONSE_OK "?OK"
#define DATA "?DATA"
#define ENDLN '\n'
/* Number of retransmission for control packets*/
#define MAX_CTRL_PACKET_TRANSMISSION 5
/* Delay timer */
#define PROBE_TIMER_DELAY 2
#define MEASURE_LINK_QUALITY_DELAY 5
#define CALCULATE_DATA_PRR_DELAY 20
/* PRR Target Define */
#define PRR_TARGET 0.99f

#define NUMBER_OF_DATA 200

uint8_t cnlab_current_state;
static node_info_t node;
static unsigned long sf_length;
static struct ctimer c_timer;
static schedule_data_t schedule_info;
static uint8_t sd_mounted;
static uint8_t count_uart_received = 0;
static uint32_t estimate_ASN = 0;
static FATFS FatFs; /* Work area (file system object) for logical drive */
FIL fil;            /* File object */
FRESULT fr;         /* FatFs return code */
NBR_TABLE(nbr_info_t, nbr_measure_list);
PROCESS(cnlab_measure_link_process, "measure link process");
PROCESS(cnlab_receive_network_info, "receive network routing and scheduling");
PROCESS(cnlab_send_data_process, "send Data Packet");


static uint16_t number_received_packet[NUMBER_OF_NODES + 1];
static uint16_t number_tx_packet_cnt = 0;
static uint16_t number_fwd_packet_cnt = 0;
static uint16_t number_tx_packet_fail = 0;
static uint16_t number_fwd_packet_fail = 0;
static rssi_handler_t rssi_handler[NUMBER_OF_NODES + 1];
static linkaddr_t ap_address[4];
static unsigned char node_id_maps[NUMBER_OF_NODES + 1];

/*---------------------------------------------------------------------------*/
static void
cnlab_mount_sd_card()
{
  /* Mount the default drive */
  fr = f_mount(&FatFs, "", 0);
  /* Create the test file */
  fr = f_open(&fil, LOG_FILENAME, FA_WRITE | FA_OPEN_APPEND);
  if(fr) {
    printf("f_open() error: %d\n", fr);
    leds_toggle(LEDS_BLUE);
    return;
  }else{
    sd_mounted = 1;
  }
  f_printf(&fil, "%s\r\n", "----------------Added new test session----------------");
  /* Close the file */
  f_close(&fil);
}
/*---------------------------------------------------------------------------*/
static void
cnlab_unmount_sd_card()
{
  /* Unmount the default drive */
  f_mount(0, "", 0);
  sd_mounted = 0;
}
/*---------------------------------------------------------------------------*/
void
cnlab_write_log_to_file(const unsigned char *data, unsigned int len)
{
  if(!sd_mounted) {
    return;
  }
  /* Create the log file */
  fr = f_open(&fil, LOG_FILENAME, FA_WRITE | FA_OPEN_APPEND);
  if(fr) {
    printf("f_open() error: %d\n", fr);
    leds_toggle(LEDS_BLUE);
    return;
  }
  /* Write the log line */
  unsigned int ret = 0;
  f_write(&fil, data, len, &ret);
  /* Close the file */
  f_close(&fil);
}
/*---------------------------------------------------------------------------*/
static void
cnlab_log_protocol_state()
{
  char buf[128];
  int buf_size = 0;
  switch (cnlab_current_state)
  {
  case CNLAB_PROTOCOL_INITIALIZE:
    LOG_INFO("CNLAB PROTOCOL INITIALIZE at ASN = %ld\r\n", tsch_current_asn.ls4b);
    buf_size = snprintf(buf, sizeof(buf), "CNLAB PROTOCOL INITIALIZE at ASN = %ld\r\n", tsch_current_asn.ls4b);
    break;
  case CNLAB_PROTOCOL_MEASURE_LINK_QUALITY:
    LOG_INFO("CNLAB PROTOCOL MEASURE LINK QUALITY at ASN = %ld\r\n", tsch_current_asn.ls4b);
    buf_size = snprintf(buf, sizeof(buf), "CNLAB PROTOCOL MEASURE LINK QUALITY at ASN = %ld\r\n", tsch_current_asn.ls4b);
    break;
  case CNLAB_PROTOCOL_MAKE_SCHEDULE:
    LOG_INFO("CNLAB PROTOCOL MAKE TSCH SCHEDULE at ASN = %ld\r\n", tsch_current_asn.ls4b);
    buf_size = snprintf(buf, sizeof(buf), "CNLAB PROTOCOL MAKE TSCH SCHEDULE at ASN = %ld\r\n", tsch_current_asn.ls4b);
    break;
  case CNLAB_PROTOCOL_SEND_DATA:
    LOG_INFO("CNLAB PROTOCOL SEND DATA at ASN = %ld\r\n", tsch_current_asn.ls4b);
    buf_size = snprintf(buf, sizeof(buf), "CNLAB PROTOCOL SEND DATA at ASN = %ld\r\n", tsch_current_asn.ls4b);
    break;
  default:
    LOG_INFO("CNLAB PROTOCOL UNDEFINE STATE at ASN = %ld\r\n", tsch_current_asn.ls4b);
    buf_size = snprintf(buf, sizeof(buf), "CNLAB PROTOCOL UNDEFINE STATE at ASN = %ld\r\n", tsch_current_asn.ls4b);
    break;
  }
  cnlab_write_log_to_file((unsigned char *)buf, buf_size);
}
/*---------------------------------------------------------------------------*/
static void
cnlab_set_protocol_state(uint16_t state)
{
  if(cnlab_current_state != state){
      cnlab_current_state = state;
      cnlab_log_protocol_state();
  }
}
/*---------------------------------------------------------------------------*/
static int
cnlab_get_fixed_point_buffer(char *buf, size_t size, fixed_point_t value)
{
  return snprintf(buf, size, "%ld.%lld\r\n", value >> SHIFT_AMOUNT
                  ,(long long)(((value & SHIFT_MASK) * pow(10, ACCURACY)) / (1 << SHIFT_AMOUNT)));
}
/*---------------------------------------------------------------------------*/
static void
cnlab_log_fixed_point_buffer(fixed_point_t value)
{
  char buf[8];
  snprintf(buf, sizeof(buf), "%ld.%lld\r\n", value >> SHIFT_AMOUNT
                  ,(long long)(((value & SHIFT_MASK) * pow(10, ACCURACY)) / (1 << SHIFT_AMOUNT)));
  printf("%s", buf);
}
/*---------------------------------------------------------------------------*/
static double
cnlab_convert_fixed_point_to_double(fixed_point_t value)
{
    return ((value >> SHIFT_AMOUNT) + (double)(value & SHIFT_MASK)/ (1 << SHIFT_AMOUNT));
}
/*---------------------------------------------------------------------------*/
static unsigned int
uart_send_bytes(const unsigned char *s, unsigned int len)
{
  unsigned int i = 0;
  while (i < len){
    uart_write_byte(0, *s++);
    i++;
  }
  uart_write_byte(0, (uint8_t)ENDLN);
  return i;
}
/*---------------------------------------------------------------------------*/
static linkaddr_t *
cnlab_get_lladdr_by_id(uint16_t id)
{
  linkaddr_t *lladdr = NULL;
  nbr_info_t *nbr = nbr_table_head(nbr_measure_list);
  while(nbr != NULL){
    if(id == nbr->id){
      lladdr = nbr_table_get_lladdr(nbr_measure_list, nbr);
    }
    nbr = nbr_table_next(nbr_measure_list, nbr);
  }
  if(lladdr == NULL && id <= 4){
    lladdr = &ap_address[id];
  }
  return lladdr;
}
/*---------------------------------------------------------------------------*/
static void
cnlab_switch_time_source_to_best_neighbor()
{
  if(!cnlab_tsch_is_ap_node){
    nbr_info_t *nbr = nbr_table_head(nbr_measure_list);
    nbr_info_t *best_nbr = NULL;
    while(nbr != NULL){
      if(nbr->level < node.level){
        if(best_nbr == NULL){
          best_nbr = nbr;
        }else{
          double nbr_prr = cnlab_convert_fixed_point_to_double(nbr->prr_fwd);
          double best_nbr_prr = cnlab_convert_fixed_point_to_double(best_nbr->prr_fwd);
          if(nbr_prr > best_nbr_prr){
            best_nbr = nbr;
          }
        }
      }
      nbr = nbr_table_next(nbr_measure_list, nbr);
    }
    /* Update time source */
    if(best_nbr != NULL) {
      char buf[128];
      int buf_size = snprintf(buf, sizeof(buf), "CNLAB switch to best nbr %d\r\n", best_nbr->id);
      cnlab_write_log_to_file((unsigned char *)buf, buf_size);
      tsch_queue_update_time_source(nbr_table_get_lladdr(nbr_measure_list, best_nbr));
      node.level = best_nbr->level + 1;
      tsch_schedule_keepalive_immediately();
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_prr_timer_callback(void *ptr)
{
  cnlab_set_protocol_state(CNLAB_PROTOCOL_MEASURE_LINK_QUALITY);
}
/*---------------------------------------------------------------------------*/
static void
cnlab_make_schedule_timer_callback(void *ptr)
{
  cnlab_set_protocol_state(CNLAB_PROTOCOL_MAKE_SCHEDULE);
  struct tsch_slotframe *sf_data;
  sf_data = tsch_schedule_add_slotframe(2, schedule_info.sf_size);
  sf_length = sf_data->size.val*(tsch_timing_us[tsch_ts_timeslot_length]/1000);
  for(int i = 0; i < schedule_info.num_transmit_cells; i ++){
    tsch_schedule_add_link(sf_data,
                          LINK_OPTION_TX,
                          LINK_TYPE_NORMAL, &schedule_info.transmit_cells[i].lladdr,
                          schedule_info.transmit_cells[i].timeslot,
                          schedule_info.transmit_cells[i].channel);  
  }
  heapmem_free(schedule_info.transmit_cells);
  for (int j = 0; j < schedule_info.num_receive_cells; j++){
    tsch_schedule_add_link(sf_data,
                          LINK_OPTION_RX,
                          LINK_TYPE_NORMAL, &schedule_info.receive_cells[j].lladdr,
                          schedule_info.receive_cells[j].timeslot,
                          schedule_info.receive_cells[j].channel);
  }
  heapmem_free(schedule_info.receive_cells);
  struct tsch_slotframe *sf = tsch_schedule_get_slotframe_by_handle(0);
  if(1 == tsch_schedule_remove_slotframe(sf)){
    LOG_INFO("CNLAB Removed Orchestra Slotframe Successful\r\n");
  }else{
    LOG_INFO("CNLAB Removed Orchestra Slotframe Error\r\n");
  }
  if(!cnlab_tsch_is_ap_node){
    //Update timesource to Primary Parent
    linkaddr_t *lladdr = cnlab_get_lladdr_by_id(node.primary_parent);
    if(lladdr != NULL){
      tsch_queue_update_time_source(lladdr);
    }else
    {
      LOG_INFO("CNLAB Can not change timesource to P-Parent\r\n");
    }
    //Start process to run data transmission
    process_start(&cnlab_send_data_process, NULL);
  }else{
    //Exit cnlab_receive_network_info process
    process_exit(&cnlab_receive_network_info);
  }
  LOG_INFO("CNLAB Finish Update Schedule\r\n");
}
/*---------------------------------------------------------------------------*/
static int
cnlab_is_available_neighbor(uint16_t id)
{
  for(uint8_t i = 1; i <= node.max_avai_neighbor; i++){
    if(id == rssi_handler[i].id){
      return 1;
    }
  }
  return 0;
}
/*---------------------------------------------------------------------------*/
static void
cnlab_start_measure_link_quality(uint16_t id, uint8_t level, int16_t rssi, uint16_t seqno, const linkaddr_t *lladdr)
{
  if(!cnlab_is_available_neighbor(id)){
    return;
  }
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, lladdr);
  if(nbr != NULL && nbr->id == id){
    nbr->avg_rssi = ((nbr->avg_rssi*nbr->rx_probe_count + rssi)/(nbr->rx_probe_count + 1));
    nbr->rx_probe_count++;
    LOG_INFO("CNLAB received %d Probe seqno %d from Node %d Level %d AVG_RSSI %d at ASN = %ld\r\n",nbr->rx_probe_count, seqno, nbr->id, nbr->level, nbr->avg_rssi, tsch_current_asn.ls4b);
  }else if(node.degree == NBR_TABLE_MAX_NEIGHBORS){
    LOG_INFO("CNLAB reach max number of neighbors\r\n");
  }else{
    nbr = (nbr_info_t *)nbr_table_add_lladdr(nbr_measure_list, lladdr, NBR_TABLE_REASON_MAC, NULL);
    if(nbr != NULL){
      LOG_INFO("CNLAB added new neighbor id %d level %d seqno %d RSSI %d at ASN = %ld\r\n", id, level, seqno, rssi, tsch_current_asn.ls4b);
      char buf[127];
      int buf_size = snprintf(buf, sizeof(buf), "CNLAB added new neighbor id %d level %d seqno %d RSSI %d at ASN = %ld\r\n", id, level, seqno, rssi, tsch_current_asn.ls4b);
      cnlab_write_log_to_file((unsigned char *)buf, buf_size);
      node.degree++;
      nbr->id = id;
      nbr->level = level;
      nbr->rx_probe_count++;
      nbr->avg_rssi = rssi;
    }
  }
}
/*---------------------------------------------------------------------------*/
void
cnlab_protocol_reset()
{
  cnlab_set_protocol_state(CNLAB_PROTOCOL_RESET);
  node.id = node_id;
  node.type = NODE;
  node.degree = 0;
  sf_length = 0;
  nbr_info_t *nbr;
  /* Remove all nbr stats */
  nbr = nbr_table_head(nbr_measure_list);
  while(nbr != NULL) {
    nbr_table_remove(nbr_measure_list, nbr);
    nbr = nbr_table_next(nbr_measure_list, nbr);
  }
  cnlab_unmount_sd_card();
}
/*---------------------------------------------------------------------------*/
void
cnlab_protocol_init()
{
  cnlab_current_state = CNLAB_PROTOCOL_DEFAULT;
  node.id = node_id;
  node.type = NODE;
  node.degree = 0;
  sf_length = 0;
  for(uint8_t i = 1; i < NUMBER_OF_NODES + 1; i++){
    rssi_handler[i].id = i;
    rssi_handler[i].count = 0;
    rssi_handler[i].rssi_avg = -256;
    node_id_maps[i] = (unsigned char)96 + i;
  }
  nbr_table_register(nbr_measure_list, NULL);
  cnlab_set_protocol_state(CNLAB_PROTOCOL_INITIALIZE);
  //send_data_event = process_alloc_event();
  memset(&number_received_packet, 0, sizeof(number_received_packet));
}
/*---------------------------------------------------------------------------*/
static void
cnlab_probe_message_callback(void *ptr, int status, int transmissions)
{
  LOG_INFO("CNLAB Node %d broadcast Probe packet at ASN = %ld", node.id, tsch_current_asn.ls4b);
  LOG_INFO_(", st %d-%d\r\n", status, transmissions);
  ctimer_set(&c_timer, MEASURE_LINK_QUALITY_DELAY*sf_length*CLOCK_MILISEC, cnlab_prr_timer_callback, NULL);
}
/*---------------------------------------------------------------------------*/
static void
cnlab_probe_message_output(uint8_t packet_type, uint16_t seqno)
{
  packetbuf_clear();
  uint8_t *p = packetbuf_dataptr();
  p[0] = packet_type;
  p[1] = node.id;
  p[2] = node.id >> 8;
  p[3] = node.type;
  p[4] = tsch_join_priority; //tree level
  p[5] = seqno;
  p[6] = seqno >> 8;
  packetbuf_set_datalen(7);
  packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, CNLABFRAME_PROBE);
  send_control_packet(cnlab_probe_message_callback, NULL);
}
/*---------------------------------------------------------------------------*/
void
cnlab_probe_message_input()
{
  uint8_t *p = packetbuf_dataptr();
  uint8_t packet_type = p[0];
  uint16_t id = p[1]|p[2] << 8;
  uint8_t node_type = p[3];
  uint8_t level = p[4];
  uint16_t seqno = p[5]|p[6] << 8;
  int16_t rssi = (int16_t)packetbuf_attr(PACKETBUF_ATTR_RSSI);
  if(packet_type == PROBE_RSSI){
    if(!(node.type == AP && node_type == AP)){
      rssi_handler[id].rssi_avg = ((rssi_handler[id].rssi_avg*rssi_handler[id].count + rssi)/(rssi_handler[id].count + 1));
      rssi_handler[id].count++;
    }else{
      if(id == 1){ //AP1
        tsch_queue_update_time_source(packetbuf_addr(PACKETBUF_ADDR_SENDER));
        tsch_join_priority = level;
      }
      ap_address[id] = *packetbuf_addr(PACKETBUF_ADDR_SENDER);
    }
  }else if(packet_type == PROBE_PRR){
    if(!(node.type == AP && node_type == AP)){
      cnlab_start_measure_link_quality(id, level, rssi, seqno, packetbuf_addr(PACKETBUF_ADDR_SENDER));
    }
  }
}

/*---------------------------------------------------------------------------*/
static void
cnlab_prr_message_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t*)ptr);
  if(nbr != NULL){
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB PRR sent to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }
  if(status != MAC_TX_OK){
    int is_removed = nbr_table_remove(nbr_measure_list, nbr);
    if(is_removed){
      node.degree--;
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_prr_message_output(uint16_t nbr_id, fixed_point_t prr, linkaddr_t *lladdr)
{
  packetbuf_clear();
  uint8_t *p = packetbuf_dataptr();
  p[0] = nbr_id;
  p[1] = nbr_id >> 8;
  p[2] = prr;
  p[3] = prr >> 8;
  p[4] = prr >> 16;
  p[5] = prr >> 24;
  packetbuf_set_datalen(6);
  packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, CNLABFRAME_PRR);
  packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
  packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
  send_control_packet(cnlab_prr_message_callback, lladdr);
}
/*---------------------------------------------------------------------------*/
void
cnlab_prr_message_input()
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, packetbuf_addr(PACKETBUF_ADDR_SENDER));
  uint8_t *p = packetbuf_dataptr();
  if(nbr != NULL){
    if(node.id == (p[0]|p[1]<<8)){
      nbr->prr_fwd = (p[2]|p[3]<<8|p[4]<<16|p[5]<<24);
      char buf[128];
      int buf_size = snprintf(buf, sizeof(buf), "Received from Node %d PRR ", nbr->id);
      buf_size += cnlab_get_fixed_point_buffer(buf + buf_size, sizeof(buf), nbr->prr_fwd);
      cnlab_write_log_to_file((unsigned char *)buf, buf_size);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_node_information_forward_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t *)(ptr));
  if(nbr != NULL){
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Node Information forward to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }else{
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Node Information forwarded, st %d, trans %d\r\n", status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_node_information_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t *)(ptr));
  if(nbr != NULL){
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Node Information sent to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }else{
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Node Information sent, st %d, trans %d\r\n", status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }
}
/*---------------------------------------------------------------------------*/
void
cnlab_node_information_input()
{
  if(!cnlab_tsch_is_ap_node) {
    LOG_INFO("CNLAB forward Node Information to next hop\r\n");
    struct tsch_neighbor *n = tsch_queue_get_time_source();
    if(n != NULL) {
      int buf_size = packetbuf_datalen();
      char buffer[buf_size];
      memcpy(buffer, packetbuf_dataptr(), buf_size);
      packetbuf_copyfrom(buffer, buf_size);
      packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, CNLABFRAME_NODE_INFORMATION);
      packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
      packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, &n->addr);
      send_control_packet(cnlab_node_information_forward_callback, &n->addr);
    }else{
        char buf[127];
        uint8_t size = snprintf(buf, sizeof(buf), "No Temporary Parent - Node Information not sent\r\n");
        cnlab_write_log_to_file((unsigned char *)buf, size);
    }
  }else{
    uart_send_bytes((uint8_t *)packetbuf_dataptr(), packetbuf_datalen());
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_node_information_output()
{
  uint16_t nbr_id[NBR_TABLE_MAX_NEIGHBORS];
  fixed_point_t nbr_prr[NBR_TABLE_MAX_NEIGHBORS];
  static int length = 0;
  nbr_info_t *nbr = nbr_table_head(nbr_measure_list);
  while(nbr != NULL){
    nbr_id[length] = nbr->id;
    nbr_prr[length] = nbr->prr_bwd;
    length++;
    nbr = nbr_table_next(nbr_measure_list, nbr);
  }
  char buf[127];
  uint8_t size = snprintf(buf, sizeof(buf), "Number of neigbors %d\r\n", length);
  cnlab_write_log_to_file((unsigned char *)buf, size);
  char buffer[127];
  uint8_t buf_size = 0; 
  memset(buffer, 0, sizeof(buffer));
  buf_size = snprintf(buffer, sizeof(buffer), "%s/", (uint8_t *)NODE_INFO);
  buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%c/", node_id_maps[node.id]);
  buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%d/", node.type);
  buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%d/", length);
  for(uint8_t i = 0;  i < length; i++){
    buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%c", node_id_maps[nbr_id[i]]);
  }
  buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%c",'/');
  for(uint8_t i = 0;  i < length; i++){
    if(i < length - 1){
      buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%lld ", (long long)(((nbr_prr[i] & SHIFT_MASK) * pow(10, ACCURACY)) / (1 << SHIFT_AMOUNT)));
    }else{
      buf_size += snprintf(buffer + buf_size, sizeof(buffer), "%lld", (long long)(((nbr_prr[i] & SHIFT_MASK) * pow(10, ACCURACY)) / (1 << SHIFT_AMOUNT)));
    }
  }
  size = snprintf(buf, sizeof(buf), "Size of packet %d\r\n", buf_size);
  cnlab_write_log_to_file((unsigned char *)buf, size);
  if(!cnlab_tsch_is_ap_node) {
    struct tsch_neighbor *n = tsch_queue_get_time_source();
    if(n != NULL) {
      packetbuf_copyfrom(buffer, buf_size);
      packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, CNLABFRAME_NODE_INFORMATION);
      packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
      packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, &n->addr);
      send_control_packet(cnlab_node_information_callback, &n->addr);
    }else{
        char buf[127];
        uint8_t size = snprintf(buf, sizeof(buf), "No Temporary Parent - Node Information not sent\r\n");
        cnlab_write_log_to_file((unsigned char *)buf, size);
    }
  }else{
    uart_send_bytes((uint8_t *)buffer, buf_size);
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_exchanged_prr_timer_callback(void *ptr)
{
  LOG_INFO("CNLAB exchanged PRR timer callback at ASN = %ld\r\n", tsch_current_asn.ls4b);
  char buf[128];
  int buf_size = snprintf(buf, sizeof(buf), "CNLAB exchanged PRR timer callback at ASN = %ld\r\n", tsch_current_asn.ls4b);
  cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  nbr_info_t *nbr = nbr_table_head(nbr_measure_list);
  while (nbr != NULL)
  {
    double prr_fwd = cnlab_convert_fixed_point_to_double(nbr->prr_fwd);
    if(prr_fwd < MIN_PRR_LINK){
      int is_removed = nbr_table_remove(nbr_measure_list, nbr);
      if(is_removed){
        node.degree--;
      }
    }
    nbr = nbr_table_next(nbr_measure_list, nbr);
  }
  cnlab_switch_time_source_to_best_neighbor();
  cnlab_node_information_output();
  if(cnlab_tsch_is_ap_node){
    process_start(&cnlab_receive_network_info, NULL);
  }
}
/*---------------------------------------------------------------------------*/
static void swap(rssi_handler_t* a, rssi_handler_t* b) 
{ 
    rssi_handler_t t = *a; 
    *a = *b; 
    *b = t; 
}
/*---------------------------------------------------------------------------*/
static int partition (rssi_handler_t arr[], int low, int high) 
{ 
    int pivot = arr[high].rssi_avg;    // pivot 
    int i = (low - 1);  // Index of smaller element 
  
    for (int j = low; j <= high- 1; j++) 
    { 
        // If current element is greater than the pivot 
        if (arr[j].rssi_avg > pivot) 
        { 
            i++;    // increment index of smaller element 
            swap(&arr[i], &arr[j]); 
        } 
    } 
    swap(&arr[i + 1], &arr[high]); 
    return (i + 1); 
}
/*---------------------------------------------------------------------------*/
static void quickSort(rssi_handler_t arr[], int low, int high) 
{ 
    if (low < high) 
    { 
        /* pi is partitioning index, arr[p] is now 
           at right place */
        int pi = partition(arr, low, high); 
  
        // Separately sort elements before 
        // partition and after partition 
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    } 
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(cnlab_measure_link_process, ev, data)
{
  static struct etimer lq_timer;
  static struct tsch_slotframe *sf;
  static uint16_t seqno = 1;
  PROCESS_BEGIN();
  etimer_set(&lq_timer, 10*CLOCK_MILISEC);
  //Wait until associate
  while (!tsch_is_associated){
    PROCESS_YIELD_UNTIL(etimer_expired(&lq_timer));
    etimer_reset(&lq_timer);
  }
  sf = tsch_schedule_get_slotframe_by_handle(0);
  sf_length = sf->size.val*(tsch_timing_us[tsch_ts_timeslot_length]/1000);
  LOG_INFO("CNLAB current ASN = %ld sf size = %d\r\n", tsch_current_asn.ls4b, sf->size.val);
  char buf[127];
  int buf_size = snprintf(buf, sizeof(buf), "CNLAB current ASN = %ld sf size = %d\r\n", tsch_current_asn.ls4b, sf->size.val);
  cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  LOG_INFO("CNLAB Slotframe length = %ld NUMBER_OF_PROBE_PRR = %d\r\n", sf_length, NUMBER_OF_PROBE_PRR);
  buf_size = snprintf(buf, sizeof(buf), "CNLAB Slotframe length = %ld NUMBER_OF_PROBE_PRR = %d\r\n", sf_length, NUMBER_OF_PROBE_PRR);
  cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  LOG_INFO("CNLAB WAITING FOR ALL NODES JOIN THE NETWORK\r\n");
  //Create EBs delay base on device performance
  tsch_set_eb_period(sf_length*CLOCK_MILISEC);
  //Delay SEND_EB_DELAY*MAX_DEPTH*JOIN_HOPPING SFs to wait all nodes receive EBs and join the network
  while (tsch_current_asn.ls4b < MAX_DEPTH*sizeof(TSCH_JOIN_HOPPING_SEQUENCE)*sf->size.val)
  {
    PROCESS_YIELD_UNTIL(etimer_expired(&lq_timer));
    etimer_reset(&lq_timer);
  }
  LOG_INFO("CNLAB END OF SEND EB at ASN = %ld\r\n", tsch_current_asn.ls4b);
  // End EB process
  process_exit(&tsch_send_eb_process);
  // Delay time before send Probe packet.
  etimer_set(&lq_timer, PROBE_TIMER_DELAY*sf_length*CLOCK_MILISEC);
  PROCESS_YIELD_UNTIL(etimer_expired(&lq_timer));
  etimer_reset(&lq_timer);
  // Create NUMBER_OF_PROBE_RSSI packets to choose neighbors
  while(seqno <= NUMBER_OF_PROBE_RSSI){
    PROCESS_YIELD_UNTIL(etimer_expired(&lq_timer));
    cnlab_probe_message_output(PROBE_RSSI, seqno);
    seqno++;
    etimer_reset(&lq_timer);
  }
  quickSort(rssi_handler, 1, NUMBER_OF_NODES);
  for(uint8_t i = 1; i <= NBR_TABLE_MAX_NEIGHBORS; i++){
    if(rssi_handler[i].count > 0){
      node.max_avai_neighbor++;
    }
  }
  seqno = 1;
  // Create NUMBER_OF_PROBE_PRR packets for calculate PRR
  while(seqno <= NUMBER_OF_PROBE_PRR)
  {
    PROCESS_YIELD_UNTIL(etimer_expired(&lq_timer));
    cnlab_probe_message_output(PROBE_PRR, seqno);
    seqno++;
    etimer_reset(&lq_timer);
  }
  while(cnlab_current_state != CNLAB_PROTOCOL_MEASURE_LINK_QUALITY) {
    PROCESS_YIELD_UNTIL(etimer_expired(&lq_timer));
    etimer_reset(&lq_timer);
  }
  ctimer_set(&c_timer, (MAX_CTRL_PACKET_TRANSMISSION*NBR_TABLE_MAX_NEIGHBORS)*sf_length*CLOCK_MILISEC, cnlab_exchanged_prr_timer_callback, NULL);
  nbr_info_t *nbr = nbr_table_head(nbr_measure_list);
  while(nbr != NULL){
    nbr->prr_bwd = (fixed_point_t)(nbr->rx_probe_count << SHIFT_AMOUNT);
    nbr->prr_bwd /= NUMBER_OF_PROBE_PRR;
    double prr_bwd = cnlab_convert_fixed_point_to_double(nbr->prr_bwd);
    if(prr_bwd >= MAX_PRR_LINK){
      nbr->prr_bwd = (1 << SHIFT_AMOUNT)*MAX_PRR_LINK;
    }
    LOG_INFO("CNLAB PRR(%d->%d) = ", nbr->id, node.id);
    cnlab_log_fixed_point_buffer(nbr->prr_bwd);
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB PRR(%d->%d) = ", nbr->id, node.id);
    buf_size += cnlab_get_fixed_point_buffer(buf + buf_size, sizeof(buf), nbr->prr_bwd);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
    linkaddr_t *lladdr = nbr_table_get_lladdr(nbr_measure_list, nbr);
    if(prr_bwd > MIN_PRR_LINK){
      cnlab_prr_message_output(nbr->id, nbr->prr_bwd, lladdr);
    }else{
      int is_removed = nbr_table_remove(nbr_measure_list, nbr);
      if(is_removed){
        node.degree--;
      }
    }
    nbr = nbr_table_next(nbr_measure_list, nbr);
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
static void
cnlab_send_routing_packet_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t *)(ptr));
  if(nbr != NULL){
    LOG_INFO("CNLAB Routing Infomation sent to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Routing Infomation sent to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }else{
    LOG_INFO("CNLAB Routing Infomation sent, st %d, trans %d\r\n", status, transmissions);
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Routing Infomation sent, st %d, trans %d\r\n", status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_send_schedule_packet_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t *)(ptr));
  if(nbr != NULL){
    LOG_INFO("CNLAB Schedule Infomation sent to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Schedule Infomation sent to Node %d, st %d, trans %d\r\n", nbr->id, status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }else{
    LOG_INFO("CNLAB Schedule Infomation sent, st %d, trans %d\r\n", status, transmissions);
    char buf[128];
    int buf_size = snprintf(buf, sizeof(buf), "CNLAB Schedule Infomation sent, st %d, trans %d\r\n", status, transmissions);
    cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  }
}
/*---------------------------------------------------------------------------*/
void cnlab_routing_data_input()
{
  LOG_INFO("CNLAB Routing Infomation received\r\n");
  uint8_t *buf = packetbuf_dataptr();
  uint8_t cur_len = 0;
  uint8_t pathData_size = buf[0];
  LOG_INFO("pathData size %d\r\n", pathData_size);
  cur_len += 1;
  uint8_t pathData[pathData_size];
  memset(&pathData, 0, sizeof(pathData));
  for(int i = 0; i < pathData_size; i++) {
    pathData[i] = buf[cur_len + i];
    LOG_INFO("pathData %d\r\n", pathData[i]);
  }
  cur_len += pathData_size;
  uint8_t length = buf[cur_len];
  cur_len += 1;
  uptree_info_t uptree[length];
  for(int i = 0; i < length; i++){
    uptree[i].node = buf[cur_len];
    uptree[i].rank = (buf[cur_len + 1]|buf[cur_len + 2] << 8|buf[cur_len + 3] << 16|buf[cur_len + 4] << 24);
    uptree[i].primary_parent = buf[cur_len + 5];
    uptree[i].reserve_parent = buf[cur_len + 6];
    LOG_INFO("Node %d P_Parent %d R_Parent %d Rank ", uptree[i].node
            ,uptree[i].primary_parent, uptree[i].reserve_parent);
    cnlab_log_fixed_point_buffer(uptree[i].rank);
    if(node.id == uptree[i].node) {
      node.rank = uptree[i].rank;
      node.primary_parent = uptree[i].primary_parent;
      node.reserve_parent = uptree[i].reserve_parent;
    }
    cur_len += 7;
  }
  if(pathData_size - 1 > 0){
    packetbuf_clear();
    uint8_t *p = packetbuf_dataptr();
    uint8_t p_len = 0;
    p[0] = pathData_size - 1;
    p_len += 1;
    uint8_t next_path = 0;
    if(pathData[0] == node.id){
      next_path = pathData[1];
      LOG_INFO("next path %d\r\n",next_path);
    }
    for(int i = 0; i < pathData_size - 1; i++){
      p[p_len + i] = pathData[i + 1];
    }
    p_len += pathData_size - 1;
    p[p_len] = length;
    p_len += 1;
    for(int i = 0; i < length; i++){
      p[p_len] = uptree[i].node;
      fixed_point_t rank = uptree[i].rank;
      p[p_len + 1] = rank;
      p[p_len + 2] = rank >> 8;
      p[p_len + 3] = rank >> 16;
      p[p_len + 4] = rank >> 24;
      p[p_len + 5] = uptree[i].primary_parent;
      p[p_len + 6] = uptree[i].reserve_parent;
      p_len += 7;
    }
    linkaddr_t *lladdr = cnlab_get_lladdr_by_id(next_path);
    packetbuf_set_datalen(p_len);
    packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, CNLABFRAME_ROUTING);
    packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
    packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
    send_control_packet(cnlab_send_routing_packet_callback, lladdr);
  }
}
/*---------------------------------------------------------------------------*/
void cnlab_schedule_data_input()
{
  LOG_INFO("CNLAB Schedule Information received\r\n");
  uint8_t *p = packetbuf_dataptr();
  uint8_t cur_len = 0;
  uint8_t pathData_size = p[0];
  LOG_INFO("Path size: %d\r\n", pathData_size);
  uint8_t num_frags = p[1] >> 4;
  LOG_INFO("Number of fragment %d\r\n", num_frags);
  uint8_t frag_index = 0;
  if(num_frags > 0){
      frag_index = p[1] ^ (num_frags << 4);
      LOG_INFO("Frag index %d\r\n", frag_index);
  }
  schedule_info.sf_size = p[2]|p[3] << 8;
  LOG_INFO("SF size %d\r\n", schedule_info.sf_size);
  uint32_t estimate_ASN = 0;
  estimate_ASN = (uint32_t)p[4];
  estimate_ASN |= (uint32_t)p[5] << 8;
  estimate_ASN |= (uint32_t)p[6] << 16;
  estimate_ASN |= (uint32_t)p[7] << 24;
  cur_len += 8;
  LOG_INFO("CNLAB current ASN %ld\r\n", tsch_current_asn.ls4b);
  LOG_INFO("CNLAB estimate ASN %ld\r\n", estimate_ASN);
  uint8_t pathData[pathData_size];
  memset(&pathData, 0, sizeof(pathData));
  for(int i = 0; i < pathData_size; i++){
    pathData[i] = p[cur_len + i];
    LOG_INFO("pathData %d\r\n", pathData[i]);
  }
  uint8_t next_path = 0;
  if(pathData[0] == node.id){
    next_path = pathData[1];
    LOG_INFO("next_path %d\r\n",next_path);
  }
  cur_len += pathData_size;
  uint8_t sender = p[cur_len];
  uint8_t receiver = p[cur_len + 1];
  uint8_t num_trans = p[cur_len + 2];
  LOG_INFO("(%d, %d, %d)\r\n", sender, receiver, num_trans);
  cur_len += 3;
  cell_t cells[num_trans];
  uint16_t *tsch_data = heapmem_alloc(num_trans*sizeof(uint16_t));
  for(int i = 0; i < num_trans; i++){
    tsch_data[i] = p[cur_len]|(p[cur_len + 1] << 8);
    cells[i].timeslot = tsch_data[i] >> 4;
    cells[i].channel = tsch_data[i] ^ (cells[i].timeslot << 4);
    LOG_INFO("(%d, %d)\r\n", cells[i].timeslot, cells[i].channel);
    linkaddr_t *lladdr = NULL;
    if(node.id == sender){
      lladdr = cnlab_get_lladdr_by_id(receiver);
    }else{
      lladdr = cnlab_get_lladdr_by_id(sender);
    }
    linkaddr_copy(&cells[i].lladdr, lladdr);
    cur_len += 2;
  }
  LOG_INFO("Size of original packet is %d\r\n", cur_len);
    if(node.id == sender){
    LOG_INFO("Only save data\r\n");
    if(schedule_info.num_transmit_cells == 0){
      LOG_INFO("alloc %d bytes\r\n", sizeof(cells));
      schedule_info.transmit_cells = (cell_t *)heapmem_alloc(sizeof(cells));
    }else{
      LOG_INFO("realloc %d bytes\r\n", schedule_info.num_transmit_cells*sizeof(cell_t) + sizeof(cells));
      schedule_info.transmit_cells = (cell_t *)heapmem_realloc(schedule_info.transmit_cells, schedule_info.num_transmit_cells*sizeof(cell_t) + sizeof(cells));
    }
    memcpy(schedule_info.transmit_cells + schedule_info.num_transmit_cells, cells, sizeof(cells));
    schedule_info.num_transmit_cells += num_trans;
  }else{
    packetbuf_clear();
    uint8_t *p = packetbuf_dataptr();
    uint8_t p_len = 0;
    p[0] = pathData_size - 1;
    p[1] = (num_frags << 4)|frag_index;
    p[2] = schedule_info.sf_size;
    p[3] = schedule_info.sf_size >> 8;
    p[4] = estimate_ASN;
    p[5] = estimate_ASN >> 8;
    p[6] = estimate_ASN >> 16;
    p[7] = estimate_ASN >> 24;
    p_len += 8;
    uint8_t next_path = 0;
    if(pathData[0] == node.id){
      next_path = pathData[1];
      LOG_INFO("next path %d\r\n",next_path);
    }
    for(int i = 0; i < pathData_size - 1; i++){
      p[p_len + i] = pathData[i + 1];
    }
    p_len += pathData_size - 1;
    p[p_len] = sender;
    p[p_len + 1] = receiver;
    p[p_len + 2] = num_trans;
    p_len += 3;
    for(int i = 0; i < num_trans; i++){
      p[p_len] = tsch_data[i];
      p[p_len + 1] = tsch_data[i] >> 8;
      p_len += 2;
    }
    linkaddr_t *lladdr = cnlab_get_lladdr_by_id(next_path);
    LOG_INFO("Size of forward packet is %d\r\n", p_len);
    if(node.id == receiver){
      LOG_INFO("Save data and then forward data\r\n");
      if(schedule_info.num_receive_cells == 0){
        LOG_INFO("alloc %d bytes\r\n", sizeof(cells));
        schedule_info.receive_cells = (cell_t *)heapmem_alloc(sizeof(cells));
      }else{
        LOG_INFO("realloc %d bytes\r\n", schedule_info.num_receive_cells*sizeof(cell_t) + sizeof(cells));
        schedule_info.receive_cells = (cell_t *)heapmem_realloc(schedule_info.receive_cells, schedule_info.num_receive_cells*sizeof(cell_t) + sizeof(cells));
      }
      memcpy(schedule_info.receive_cells + schedule_info.num_receive_cells, cells, sizeof(cells));
      schedule_info.num_receive_cells += num_trans;
    }else{
      LOG_INFO("Only forward data\r\n");
    }
    packetbuf_set_datalen(p_len);
    packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_CMDFRAME);
    packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
    packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
    send_control_packet(cnlab_send_schedule_packet_callback, lladdr);
  }
  heapmem_free(tsch_data);
  ctimer_set(&c_timer, (estimate_ASN - tsch_current_asn.ls4b)*(tsch_timing_us[tsch_ts_timeslot_length]/1000)*CLOCK_MILISEC, cnlab_make_schedule_timer_callback, NULL);
}
/*---------------------------------------------------------------------------*/
static void process_uart_routing_data(const uint8_t *buf)
{
  uint8_t cur_len = 0;
  uint8_t pathData_size = buf[0];
  LOG_INFO("pathData size %d\r\n", pathData_size);
  cur_len += 1;
  uint8_t pathData[pathData_size];
  memset(&pathData, 0, sizeof(pathData));
  for(int i = 0; i < pathData_size; i++) {
    pathData[i] = buf[cur_len + i];
    LOG_INFO("pathData %d\r\n", pathData[i]);
  }
  cur_len += pathData_size;
  uint8_t length = buf[cur_len];
  cur_len += 1;
  uptree_info_t uptree[length];
  for(int i = 0; i < length; i++){
    uptree[i].node = buf[cur_len];
    uptree[i].rank = (buf[cur_len + 1]|buf[cur_len + 2] << 8|buf[cur_len + 3] << 16|buf[cur_len + 4] << 24);
    uptree[i].primary_parent = buf[cur_len + 5];
    uptree[i].reserve_parent = buf[cur_len + 6];
    if(node.id == uptree[i].node) {
      node.rank = uptree[i].rank;
      node.primary_parent = uptree[i].primary_parent;
      node.reserve_parent = uptree[i].reserve_parent;
      LOG_INFO("Node %d P_Parent %d R_Parent %d Rank ", uptree[i].node, uptree[i].primary_parent, uptree[i].reserve_parent);
      cnlab_log_fixed_point_buffer(uptree[i].rank);
    }
    cur_len += 7;
  }
  if(pathData_size - 1 > 0){
    packetbuf_clear();
    uint8_t *p = packetbuf_dataptr();
    uint8_t p_len = 0;
    p[0] = pathData_size - 1;
    p_len += 1;
    uint8_t next_path = 0;
    if(pathData[0] == node.id){
      next_path = pathData[1];
      LOG_INFO("next path %d\r\n",next_path);
    }
    for(int i = 0; i < pathData_size - 1; i++){
      p[p_len + i] = pathData[i + 1];
    }
    p_len += pathData_size - 1;
    p[p_len] = length;
    p_len += 1;
    for(int i = 0; i < length; i++){
      p[p_len] = uptree[i].node;
      fixed_point_t rank = uptree[i].rank;
      p[p_len + 1] = rank;
      p[p_len + 2] = rank >> 8;
      p[p_len + 3] = rank >> 16;
      p[p_len + 4] = rank >> 24;
      p[p_len + 5] = uptree[i].primary_parent;
      p[p_len + 6] = uptree[i].reserve_parent;
      p_len += 7;
    }
    linkaddr_t *lladdr = cnlab_get_lladdr_by_id(next_path);
    packetbuf_set_datalen(p_len);
    packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, CNLABFRAME_ROUTING);
    packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
    packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
    send_control_packet(cnlab_send_routing_packet_callback, lladdr);
  }
}
/*---------------------------------------------------------------------------*/
static void process_uart_scheduling_data(const uint8_t *buf)
{
  uint8_t cur_len = 0;
  uint8_t pathData_size = buf[0];
  LOG_INFO("Path size: %d\r\n", pathData_size);
  uint8_t num_frags = buf[1] >> 4;
  LOG_INFO("Number of fragment %d\r\n", num_frags);
  uint8_t frag_index = 0;
  if(num_frags > 0){
      frag_index = buf[1] ^ (num_frags << 4);
      LOG_INFO("Frag index %d\r\n", frag_index);
  }
  schedule_info.sf_size = buf[2]|buf[3] << 8;
  LOG_INFO("SF size %d\r\n", schedule_info.sf_size);
  if(count_uart_received == 1){
    uint32_t estimate_download_time = 0;
    estimate_download_time  = (uint32_t)buf[4];
    estimate_download_time |= (uint32_t)buf[5] << 8;
    estimate_download_time |= (uint32_t)buf[6] << 16;
    estimate_download_time |= (uint32_t)buf[7] << 24;
    LOG_INFO("CNLAB current ASN %ld\r\n", tsch_current_asn.ls4b);
    estimate_ASN = tsch_current_asn.ls4b + estimate_download_time/(tsch_timing_us[tsch_ts_timeslot_length]/1000);
    LOG_INFO("CNLAB estimate ASN %ld\r\n", estimate_ASN);
    ctimer_set(&c_timer, (estimate_ASN - tsch_current_asn.ls4b)*(tsch_timing_us[tsch_ts_timeslot_length]/1000)*CLOCK_MILISEC, cnlab_make_schedule_timer_callback, NULL);
  }
  cur_len += 8;
  uint8_t pathData[pathData_size];
  memset(&pathData, 0, sizeof(pathData));
  for(int i = 0; i < pathData_size; i++){
    pathData[i] = buf[cur_len + i];
    LOG_INFO("pathData %d\r\n", pathData[i]);
  }
  cur_len += pathData_size;
  uint8_t sender = buf[cur_len];
  uint8_t receiver = buf[cur_len + 1];
  uint8_t num_trans = buf[cur_len + 2];
  LOG_INFO("(%d, %d, %d)\r\n", sender, receiver, num_trans);
  cur_len += 3;
  cell_t cells[num_trans];
  memset(&cells, 0, sizeof(cells));
  uint16_t *tsch_data = heapmem_alloc(num_trans*sizeof(uint16_t));
  for(int i = 0; i < num_trans; i++){
    tsch_data[i] = buf[cur_len]|(buf[cur_len + 1] << 8);
    cells[i].timeslot = tsch_data[i] >> 4;
    cells[i].channel = tsch_data[i] ^ (cells[i].timeslot << 4);
    LOG_INFO("(%d, %d)\r\n", cells[i].timeslot, cells[i].channel);
    linkaddr_t *lladdr = NULL;
    if(node.id == sender){
      lladdr = cnlab_get_lladdr_by_id(receiver);
    }else{
      lladdr = cnlab_get_lladdr_by_id(sender);
    }
    linkaddr_copy(&cells[i].lladdr, lladdr);
    cur_len += 2;
  }
  LOG_INFO("Size of original packet is %d\r\n", cur_len);
  if(node.id == sender){
    LOG_INFO("Only save data\r\n");
    if(schedule_info.num_transmit_cells == 0){
      LOG_INFO("alloc %d bytes\r\n", sizeof(cells));
      schedule_info.transmit_cells = (cell_t *)heapmem_alloc(sizeof(cells));
    }else{
      LOG_INFO("realloc %d bytes\r\n", schedule_info.num_transmit_cells*sizeof(cell_t) + sizeof(cells));
      schedule_info.transmit_cells = (cell_t *)heapmem_realloc(schedule_info.transmit_cells, schedule_info.num_transmit_cells*sizeof(cell_t) + sizeof(cells));
    }
    memcpy(schedule_info.transmit_cells + schedule_info.num_transmit_cells, cells, sizeof(cells));
    schedule_info.num_transmit_cells += num_trans;
  }else{
    packetbuf_clear();
    uint8_t *p = packetbuf_dataptr();
    uint8_t p_len = 0;
    p[0] = pathData_size - 1;
    p[1] = (num_frags << 4)|frag_index;
    p[2] = schedule_info.sf_size;
    p[3] = schedule_info.sf_size >> 8;
    p[4] = estimate_ASN;
    p[5] = estimate_ASN >> 8;
    p[6] = estimate_ASN >> 16;
    p[7] = estimate_ASN >> 24;
    p_len += 8;
    uint8_t next_path = 0;
    if(pathData[0] == node.id){
      next_path = pathData[1];
      LOG_INFO("next path %d\r\n",next_path);
    }
    for(int i = 0; i < pathData_size - 1; i++){
      p[p_len + i] = pathData[i + 1];
    }
    p_len += pathData_size - 1;
    p[p_len] = sender;
    p[p_len + 1] = receiver;
    p[p_len + 2] = num_trans;
    p_len += 3;
    for(int i = 0; i < num_trans; i++){
      p[p_len] = tsch_data[i];
      p[p_len + 1] = tsch_data[i] >> 8;
      p_len += 2;
    }
    linkaddr_t *lladdr = cnlab_get_lladdr_by_id(next_path);
    LOG_INFO("Size of forward packet is %d\r\n", p_len);
    if(node.id == receiver){
      LOG_INFO("Save data and then forward data\r\n");
      if(schedule_info.num_receive_cells == 0){
        LOG_INFO("alloc %d bytes\r\n", sizeof(cells));
        schedule_info.receive_cells = (cell_t *)heapmem_alloc(sizeof(cells));
      }else{
        LOG_INFO("realloc %d bytes\r\n", schedule_info.num_receive_cells*sizeof(cell_t) + sizeof(cells));
        schedule_info.receive_cells = (cell_t *)heapmem_realloc(schedule_info.receive_cells, schedule_info.num_receive_cells*sizeof(cell_t) + sizeof(cells));
      }
      memcpy(schedule_info.receive_cells + schedule_info.num_receive_cells, cells, sizeof(cells));
      schedule_info.num_receive_cells += num_trans;
    }else{
      LOG_INFO("Only forward data\r\n");
    }
    packetbuf_set_datalen(p_len);
    packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_CMDFRAME);
    packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, MAX_CTRL_PACKET_TRANSMISSION);
    packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
    send_control_packet(cnlab_send_schedule_packet_callback, lladdr);
  }
  heapmem_free(tsch_data);
}
/*---------------------------------------------------------------------------*/
static void process_uart_data(const uint8_t *buf){
  if(buf[0] == ROUTING_PACKET){
    process_uart_routing_data(buf + 1);
  }else if(buf[0] == SCHEDULING_PACKET){
    count_uart_received++;
    LOG_INFO("Scheduling data received over UART %d\r\n", count_uart_received);
    process_uart_scheduling_data(buf + 1);
  }else{
    LOG_INFO("Wrong UART data\r\n");
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_calculate_data_rr_timer_callback(void *ptr)
{
  LOG_INFO("CNLAB Calculate Data Reception Rate timer callback\r\n");
  char buf[128];
  int buf_size = snprintf(buf, sizeof(buf), "CNLAB number of tx packet %d, number of forward packet %d\r\n", number_tx_packet_cnt, number_fwd_packet_cnt);
  cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  buf_size = snprintf(buf, sizeof(buf), "CNLAB number of tx fail %d, number of forward fail %d\r\n", number_tx_packet_fail, number_fwd_packet_fail);
  cnlab_write_log_to_file((unsigned char *)buf, buf_size);
  for(int i = 2; i <= NUMBER_OF_NODES; i++)
  {
    if(number_received_packet[i] != 0){
      LOG_INFO("CNLAB DATA PRR(%d->%d) = ",i, node.id);
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_measure_packet_reception_rates(uint16_t id_node, uint16_t seqno)
{
  number_received_packet[id_node]++;
  data_t data_packet;
  memset(&data_packet, 0, sizeof(data_packet));
  snprintf(data_packet.cmd, 7, "%s ", (uint8_t *)DATA);
  snprintf(data_packet.node_id, 4, "%d ", id_node);
  snprintf(data_packet.seq_no, 6, "%d ", seqno);
  uart_send_bytes((uint8_t *)&data_packet, sizeof(data_packet));
  ctimer_set(&c_timer, CALCULATE_DATA_PRR_DELAY*sf_length*CLOCK_MILISEC, cnlab_calculate_data_rr_timer_callback, NULL);
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(cnlab_receive_network_info, ev, data)
{
  PROCESS_BEGIN();
  LOG_INFO("UART  process is running\r\n");
  while(1) {
    PROCESS_YIELD();
    if(ev == serial_line_event_message) {
      process_uart_data((uint8_t *)data);
      uart_send_bytes((uint8_t *)RESPONSE_OK, sizeof(RESPONSE_OK));
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
static void
cnlab_forward_data_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t *)ptr);
  if(nbr != NULL){
    LOG_INFO("CNLAB Data Packet forward to Node %d, st %d-%d\r\n", nbr->id, status, transmissions);
  }else{
    LOG_INFO("CNLAB Data Packet forwarded, st %d-%d\r\n", status, transmissions);
  }
  if(status == MAC_TX_OK){
    number_fwd_packet_cnt++;
  }else{
    number_fwd_packet_fail++;
  }
  ctimer_set(&c_timer, CALCULATE_DATA_PRR_DELAY*sf_length*CLOCK_MILISEC, cnlab_calculate_data_rr_timer_callback, NULL);
}
/*---------------------------------------------------------------------------*/
static void
cnlab_send_data_callback(void *ptr, int status, int transmissions)
{
  nbr_info_t *nbr = (nbr_info_t *)nbr_table_get_from_lladdr(nbr_measure_list, (linkaddr_t *)ptr);
  if(nbr != NULL){
    LOG_INFO("CNLAB Data Packet sent to Node %d, st %d-%d\r\n", nbr->id, status, transmissions);
  }else{
    LOG_INFO("CNLAB Data Packet sent, st %d-%d\r\n", status, transmissions);
  }
  if(status == MAC_TX_OK){
    number_tx_packet_cnt++;
  }else {
    number_tx_packet_fail++;
  }
  ctimer_set(&c_timer, CALCULATE_DATA_PRR_DELAY*sf_length*CLOCK_MILISEC, cnlab_calculate_data_rr_timer_callback, NULL);
}
/*---------------------------------------------------------------------------*/
void
cnlab_data_input()
{
  if(packetbuf_datalen() > 0){
    uint8_t *p = packetbuf_dataptr();
    uint16_t id = p[0]|p[1]<<8;
    uint16_t seqno = p[2]|p[3]<<8;
    if(id <= NUMBER_OF_NODES){
      cnlab_measure_packet_reception_rates(id, seqno);
    }
    if(!cnlab_tsch_is_ap_node){
      linkaddr_t *lladdr = cnlab_get_lladdr_by_id(node.primary_parent);
      if(lladdr != NULL) {
        int buf_size = packetbuf_datalen();
        char buffer[buf_size];
        memcpy(buffer, packetbuf_dataptr(), buf_size);
        packetbuf_copyfrom(buffer, buf_size);
        packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, 3);
        packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
        NETSTACK_MAC.send(cnlab_forward_data_callback, lladdr);
      }else{
        LOG_ERR("No parent - Data Packet not sent\r\n");
      }
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
cnlab_data_output(uint16_t seq_no)
{
  linkaddr_t *lladdr = cnlab_get_lladdr_by_id(node.primary_parent);
  if(lladdr != NULL) {
    packetbuf_clear();
    uint8_t *p = packetbuf_dataptr();
    p[0] = node.id;
    p[1] = node.id >> 8;
    p[2] = seq_no;
    p[3] = seq_no >> 8;
    packetbuf_set_datalen(4);
    packetbuf_set_attr(PACKETBUF_ATTR_MAX_MAC_TRANSMISSIONS, 3);
    packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, lladdr);
    NETSTACK_MAC.send(cnlab_send_data_callback, lladdr);
  }else{
    LOG_ERR("No parent - Data Packet not sent\r\n");
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(cnlab_send_data_process, ev, data)
{
  static struct etimer data_timer;
  static uint16_t number_packet = 1;
  PROCESS_BEGIN();
  etimer_set(&data_timer, sf_length*CLOCK_MILISEC);
  PROCESS_YIELD_UNTIL(etimer_expired(&data_timer));
  cnlab_set_protocol_state(CNLAB_PROTOCOL_SEND_DATA);
  while(number_packet <= NUMBER_OF_DATA){
    PROCESS_YIELD_UNTIL(etimer_expired(&data_timer));
    //linkaddr_t *lladdr = cnlab_get_lladdr_by_id(node.primary_parent);
    //process_post(PROCESS_BROADCAST, send_data_event, lladdr);
    cnlab_data_output(number_packet);
    etimer_reset(&data_timer);
    number_packet++;
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
void
cnlab_process_start()
{
  if(cnlab_tsch_is_ap_node){
    node.type = AP;
    node.rank = 1 << SHIFT_AMOUNT;
  }
  cnlab_mount_sd_card();
  process_start(&cnlab_measure_link_process, NULL);
}
