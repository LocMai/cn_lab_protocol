/* cnlab_eb_period rule implementation
 * 
 * Author: Huy
 * Date: 2019/09/20
 */
#include "contiki.h"
#include "orchestra.h"
#include "net/packetbuf.h"
#include "sys/node-id.h"

static uint16_t slotframe_handle = 0;
static struct tsch_slotframe *sf_eb;
static uint8_t timeslot_space = 1;
/*---------------------------------------------------------------------------*/
static uint16_t
cnlab_get_timeslot()
{
  return (node_id - 1)*timeslot_space;
}
/*---------------------------------------------------------------------------*/
static int
select_packet(uint16_t *slotframe, uint16_t *timeslot)
{
  if(slotframe != NULL) {
    *slotframe = slotframe_handle;
  }
  if(timeslot != NULL) {
    *timeslot = cnlab_get_timeslot();
  }
  return 1;
}
/*---------------------------------------------------------------------------*/
static void
init(uint16_t sf_handle)
{
  int i;
  uint16_t tx_timeslot;
  slotframe_handle = sf_handle;
  timeslot_space = CNLAB_SLOTFRAME_SIZE/NUMBER_OF_NODES;
  uint16_t sf_size = CNLAB_SLOTFRAME_SIZE;
  sf_eb = tsch_schedule_add_slotframe(slotframe_handle, sf_size);
  tx_timeslot = cnlab_get_timeslot();
  /*
   * Add a Tx link at node id
   * Add a Rx link at each available timeslot */
  for(i = 0; i < sf_size - timeslot_space; i+=timeslot_space) {
    tsch_schedule_add_link(sf_eb,
                            ((i == tx_timeslot) ? LINK_OPTION_TX : LINK_OPTION_RX),
                            LINK_TYPE_ADVERTISING, &tsch_broadcast_address,
                            i, 0);
  }
}
/*---------------------------------------------------------------------------*/
struct orchestra_rule cnlab_eb_period = {
  init,
  NULL,
  select_packet,
  NULL,
  NULL,
};
